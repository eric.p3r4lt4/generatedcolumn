package View;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import Model.*;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

/**
 * FXML Controller class
 *
 * @author Aluno
 */
public class TelaInicialController implements Initializable {

    @FXML private Label label;
    
    @FXML private TextField nome;
    @FXML private TextField email;
    @FXML private TextField senha;
    @FXML private TextField reSenha;
    
    @FXML private TextArea interesses;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void cadastrar() {
        if(senha.getText().equals(reSenha.getText())){
            Usuario u = new Usuario();
            Interesse i;
            String[] interesses = this.interesses.getText().split("-");
            int op;
            
            u.setNome(nome.getText());
            u.setEmail(email.getText());
            u.setSenha(senha.getText());
            
            op = u.insert();
            
            for (String in : interesses) {
                i = new Interesse();
                
                i.setIdUser(op);
                i.setNome(in);
                i.insert();
            }
            
            nome.setText("");
            email.setText("");
            senha.setText("");
            reSenha.setText("");
        }
    }
}

package Model;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Interesse {
    private int id;
    private int idUser;
    private String nome;
    
    public void setIdUser(int id){
        this.idUser = id;
    }
    
    public void setNome(String nome){
        this.nome = nome;
    }
    
    public void insert(){
        PreparedStatement ps;
        Conexao c = new Conexao();
        
        try{
            ps = c.getConexao().prepareStatement("insert into interesse values(int_seq.nextval,?,?)");
            
            ps.setInt(1, this.idUser);
            ps.setString(2, this.nome);
            
            ps.executeUpdate();
        }
        catch(SQLException e){
            e.printStackTrace();
        }
        finally{
            c.desconecta();
        }
    }
}

package Model;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Usuario {
    private int id;
    private String nome;
    private String email;
    private String senha;

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }
    
    public int insert(){
        PreparedStatement ps;
        Conexao c = new Conexao();
        String columnNames[] = {"iduser"}; 
        int coluna = 0;
        
        try{
            ps = c.getConexao().prepareStatement("insert into usuar values(usuario_seq.nextval,?,?,?)", columnNames);
            
            ps.setString(1, this.nome);
            ps.setString(2, this.email);
            ps.setString(3, this.senha);
            
            ps.executeUpdate();
            
            ResultSet rs = ps.getGeneratedKeys();
            
            if(rs.next()){
                coluna = rs.getInt(1);
            }
        }
        catch(SQLException e){
            e.printStackTrace();
        }
        finally{
            c.desconecta();
        }
        
        return coluna;
    }
}